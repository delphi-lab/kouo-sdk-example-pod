//
//  ViewController.swift
//  Kouotest
//
//  Created by Mobile on 14/12/22.
//

import UIKit
import Combine
import Kouo

class ViewController: UIViewController {
    
    private var loginStatusObserver: AnyCancellable?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Request Kouo Permissions
        self.requestKouoPermissions()
        
        // Fetch Kouo Emotional Insights
        loginStatusObserver =  KouoSDK.shared.$isLoggedIn.sink { newStatus in
            debugPrint("New Status", newStatus)
            if newStatus {
                debugPrint("We are now logged in -- fetching emotional insights")
                DispatchQueue.main.async {
                    self.fetchEmotionalInsights()
                }
            } else {
                debugPrint("We are not (yet) logged in")
            }
        }
    }
    
    
    func requestKouoPermissions() {
        let kouo = KouoSDK.shared
        guard kouo.isHealthDataAvailable else {
            return
        }
        if kouo.shouldRequestHealthKitAuthorization {
            do {
                try kouo.requestHealthKitAuthorization()
            } catch {
                debugPrint("Failed to request HealthKit authorization with \(error)")
            }
        }
        else{
            do {
                try kouo.requestHealthKitAuthorization()
            } catch {
                debugPrint("Failed to request HealthKit authorization with \(error)")
            }
        }
    }
   
    func fetchEmotionalInsights(){
        let sdk = KouoSDK.shared
        sdk.getEmotionalInsightsForRecentSessions { [weak self] kouoResult in
            guard let self else { return false }
            switch kouoResult {
            case .success(let insights):
                debugPrint("insights", insights)
                let mapArray = insights.map({$0.firebaseRepresentation})
                debugPrint("fetchEmotionalInsights",insights)
                return false
            case .failure(let error):
                debugPrint("fetchEmotionalInsightserror",error.localizedDescription)
                return false
            }
        }
    }
    
}


