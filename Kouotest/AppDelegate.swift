//
//	AppDelegate.swift
//	Kouotest
//
//	Created by Mobile on 14/12/22.
//

import UIKit
import Kouo
import Combine

let kouoAPIKey = "13a4d985-cb39-4b78-9472-2fa9c687cb23"
let kouoAPISecret = "e2635ed60e2f4f6db0a66097f8cfb9b7"


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	private var loginStatusObserver: AnyCancellable?
	private var authorizationStatusObserver: AnyCancellable?

	private func waitForHealthKitAuthorization() {
		let sdk = KouoSDK.shared

		authorizationStatusObserver = sdk.importer.$shouldRequestHealthKitAuthorization.sink { newStatus in
			if newStatus {
				debugPrint("HealthKit authorization not yet requested")
				return
			}

			do {
				try sdk.startDataImport()
			} catch {
				debugPrint("Failed to start initial data import with \(error)")
			}
		}
	}

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		let sdk = KouoSDK.shared

		debugPrint("Using Kouo SDK version \(sdk.version)")

		do {
			try sdk.setAPIKeyAndSecret(kouoAPIKey, kouoAPISecret)
			try sdk.setUsername("example@email.com")
			try sdk.start()
		} catch {
			debugPrint("Failed to start Kouo SDK with \(error)")
		}

		loginStatusObserver =  sdk.$isLoggedIn.sink { newStatus in
			if newStatus {
				debugPrint("We are now logged in -- wait for HK authorization")
				DispatchQueue.main.async {
					self.waitForHealthKitAuthorization()
				}
			} else {
				debugPrint("We are not (yet) logged in")
			}
		}

		return true
	}


	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
	   // MessagingPush.shared.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
	}

	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
	  //  MessagingPush.shared.application(application, didFailToRegisterForRemoteNotificationsWithError: error)
	}

//	  // PUSH NOTIFICATIONS
//	  override	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//			  MessagingPush.shared.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
//		  }
//
//	  override	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//			  MessagingPush.shared.application(application, didFailToRegisterForRemoteNotificationsWithError: error)
//		  }

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}



