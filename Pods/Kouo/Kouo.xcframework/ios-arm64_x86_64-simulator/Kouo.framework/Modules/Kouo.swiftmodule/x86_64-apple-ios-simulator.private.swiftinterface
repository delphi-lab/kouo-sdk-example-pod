// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.2 (swiftlang-5.7.2.135.5 clang-1400.0.29.51)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Kouo
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Combine
import Foundation
import HealthKit
import OSLog
import Swift
import UIKit
import _Concurrency
import _StringProcessing
import os.log
import os
public typealias KouoEmotionalInsightCallback = (_ result: Swift.Result<[Kouo.KouoEmotionalInsight], Swift.Error>) -> Swift.Bool
@_hasMissingDesignatedInitializers public class KouoSDK : Foundation.ObservableObject {
  public static var shared: Kouo.KouoSDK
  public var apiKey: Swift.String? {
    get
  }
  public var apiSecret: Swift.String? {
    get
  }
  public var username: Swift.String? {
    get
  }
  @Combine.Published @_projectedValueProperty($currentUser) public var currentUser: Kouo.KouoUser? {
    get
  }
  public var $currentUser: Combine.Published<Kouo.KouoUser?>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($currentBiologicalInfo) public var currentBiologicalInfo: Kouo.KouoBiologicalInfo? {
    get
  }
  public var $currentBiologicalInfo: Combine.Published<Kouo.KouoBiologicalInfo?>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($currentNotificationSettings) public var currentNotificationSettings: Kouo.KouoNotificationSettings? {
    get
  }
  public var $currentNotificationSettings: Combine.Published<Kouo.KouoNotificationSettings?>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($isLoggedIn) public var isLoggedIn: Swift.Bool {
    get
  }
  public var $isLoggedIn: Combine.Published<Swift.Bool>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($pendingNetworkCallCount) public var pendingNetworkCallCount: Swift.Int {
    get
  }
  public var $pendingNetworkCallCount: Combine.Published<Swift.Int>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($lastError) public var lastError: Kouo.KouoError? {
    get
  }
  public var $lastError: Combine.Published<Kouo.KouoError?>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($initialIngestionCompleted) public var initialIngestionCompleted: Swift.Bool {
    get
  }
  public var $initialIngestionCompleted: Combine.Published<Swift.Bool>.Publisher {
    get
  }
  public var version: Swift.String {
    get
  }
  public var userId: Swift.String? {
    get
  }
  public var isWaitingForNetwork: Swift.Bool {
    get
  }
  public var isHealthDataAvailable: Swift.Bool {
    get
  }
  public var isHealthDataImportInProgress: Swift.Bool {
    get
  }
  final public let importer: Kouo.KouoHealthKitImporter
  public func setAPIKeyAndSecret(_ apiKey: Swift.String, _ apiSecret: Swift.String) throws
  public func setUsername(_ username: Swift.String) throws
  public var shouldRequestHealthKitAuthorization: Swift.Bool {
    get
  }
  public func requestHealthKitAuthorization() throws
  public func start() throws
  public func registerNewUser(_ registration: Kouo.KouoRegistrationRequest) throws
  public func isEmailAddressTaken(_ email: Swift.String) -> Swift.Bool
  public func resendEmailConfirmation()
  public func login(_ credentials: Kouo.KouoLoginCredentials, completion: ((_ success: Swift.Bool) -> Swift.Void)? = nil)
  public func loginWithSavedCredentials(completion: ((_ success: Swift.Bool) -> Swift.Void)? = nil)
  public func logout()
  public func initiatePasswordReset(for email: Swift.String)
  public func setFCMToken(_ fcmToken: Swift.String, apnsToken: Foundation.Data)
  public func getBiologicalInfo()
  public func setBiologicalInfo(_ biologicalInfo: Kouo.KouoBiologicalInfo)
  public func getNotificationSettings()
  public func setNotificationSettings(_ notificationSettings: Kouo.KouoNotificationSettings)
  public func startDataImport(completion: (() -> Swift.Void)? = nil) throws
  public func initialIngestionCompletedForAllTypes() -> Swift.Bool
  public func resetLastError()
  public func getEmotionalInsightsForRecentSessions(_ completion: @escaping Kouo.KouoEmotionalInsightCallback)
  public typealias ObjectWillChangePublisher = Combine.ObservableObjectPublisher
  @objc deinit
}
public struct KouoCompanyInfo : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public enum KouoError : Swift.Error {
  case apiKeyAlreadySet
  case apiKeyNotSet
  case apiSecretNotSet
  case usernameNotSet
  case unsupportedOAuthTokenType
  case healthDataNotAvailable
  case notASampleType
  case unsupportedType
  case missingURL
  case missingHTTPMethod
  case missingUser
  case missingCredentials
  case notLoggedIn
  case invalidDate
  case invalidEmailAddress
  case invalidJWT
  case httpError(Swift.Int, Swift.String?, Swift.String?)
}
extension Kouo.KouoError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
extension Kouo.KouoError {
  public func isEmailAddressTakenError(_ email: Swift.String) -> Swift.Bool
}
public struct KouoRegistrationRequest : Swift.Encodable {
  public init(username: Swift.String, email: Swift.String, password: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct KouoLoginCredentials : Swift.Codable {
  public init(identifier: Swift.String, password: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
@_hasMissingDesignatedInitializers public class KouoHealthKitImporter : Combine.ObservableObject {
  @Combine.Published @_projectedValueProperty($isHealthDataAvailable) public var isHealthDataAvailable: Swift.Bool {
    get
  }
  public var $isHealthDataAvailable: Combine.Published<Swift.Bool>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($shouldRequestHealthKitAuthorization) public var shouldRequestHealthKitAuthorization: Swift.Bool {
    get
  }
  public var $shouldRequestHealthKitAuthorization: Combine.Published<Swift.Bool>.Publisher {
    get
  }
  @Combine.Published @_projectedValueProperty($pendingQueryCount) public var pendingQueryCount: Swift.Int {
    get
  }
  public var $pendingQueryCount: Combine.Published<Swift.Int>.Publisher {
    get
  }
  public var isHealthDataImportInProgress: Swift.Bool {
    get
  }
  public func requestHealthKitAuthorization() throws
  public typealias ObjectWillChangePublisher = Combine.ObservableObjectPublisher
  @objc deinit
}
public enum KouoBiologicalSex : Swift.String, Swift.Codable, Swift.CaseIterable {
  case male
  case female
  public init?(rawValue: Swift.String)
  public typealias AllCases = [Kouo.KouoBiologicalSex]
  public typealias RawValue = Swift.String
  public static var allCases: [Kouo.KouoBiologicalSex] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public enum KouoFitnessLevel : Swift.String, Swift.Codable, Swift.CaseIterable {
  case poor
  case belowAverage
  case average
  case aboveAverage
  case good
  case excellent
  case athlete
  public init?(rawValue: Swift.String)
  public typealias AllCases = [Kouo.KouoFitnessLevel]
  public typealias RawValue = Swift.String
  public static var allCases: [Kouo.KouoFitnessLevel] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public struct KouoBiologicalInfo : Swift.Codable {
  public let dateOfBirth: Foundation.Date
  public let sex: Kouo.KouoBiologicalSex
  public let fitnessLevel: Kouo.KouoFitnessLevel
  public init(dateOfBirth: Foundation.Date, sex: Kouo.KouoBiologicalSex, fitnessLevel: Kouo.KouoFitnessLevel)
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct KouoEmotionalInsight : Swift.Codable {
  public let timestamp: Foundation.Date
  public let focus: Swift.Double
  public let energy: Swift.Double
  public let stressAnxiety: Swift.Double
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension Kouo.KouoEmotionalInsight {
  public var firebaseRepresentation: [Swift.String : Any] {
    get
  }
}
public struct KouoUser : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct KouoNotificationSettings : Swift.Codable {
  public let hardFocus: Swift.Bool
  public let easyFocus: Swift.Bool
  public let hardStress: Swift.Bool
  public let easyStress: Swift.Bool
  public let hardEnergy: Swift.Bool
  public let easyEnergy: Swift.Bool
  public init(hardFocus: Swift.Bool, easyFocus: Swift.Bool, hardStress: Swift.Bool, easyStress: Swift.Bool, hardEnergy: Swift.Bool, easyEnergy: Swift.Bool)
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension Kouo.KouoBiologicalSex : Swift.Equatable {}
extension Kouo.KouoBiologicalSex : Swift.Hashable {}
extension Kouo.KouoBiologicalSex : Swift.RawRepresentable {}
extension Kouo.KouoFitnessLevel : Swift.Equatable {}
extension Kouo.KouoFitnessLevel : Swift.Hashable {}
extension Kouo.KouoFitnessLevel : Swift.RawRepresentable {}
