# Kouo

![Swift Badge](docs/img/swift-badge.svg)
![CocoaPods Badge](docs/img/cocoapods-badge.svg)

This is the iOS SDK for [Kouo](https://kouo.io)'s AI platform. It is meant to be integrated in iOS apps so that Kouo can perform its data collection magic on your behalf.  The SDK collects samples from [HealthKit](https://developer.apple.com/documentation/healthkit).

## Prerequisites

The SDK requires **iOS 13.0** or newer to work. While it builds for the simulator, you will need an actual iPhone for collecting HealthKit samples, ideally paired to a wearable device such as an Apple Watch.

## User Interface Considerations
As a general principle, the Kouo SDK does not have any user interface of its own, although it may bring up system-provided UI such as the Health Access panel. Any UI for interacting with Kouo services should be provided by the host app.

## Getting Started

First of all, you need to get an **API key** from Kouo. This is an opaque string that uniquely identifies your organization and app. You are going to need the API key to initialize the SDK.

## Installation

The Kouo SDK for iOS is currently available as [Swift package](https://developer.apple.com/documentation/xcode/swift-packages) or via [Cocoapods](https://cocoapods.org).

### Swift Package Manager

To import Kouo using Swift Package Manager, select the project in the Project Navigator in the Xcode side bar, then select the project in the Project section and click the **Package Dependencies** tab.  Click the plus button and enter the following URL in the text field that appears:

	https://gitlab.com/kouo/kouo-ios-sdk.git

As a **Dependency Rule**, we recommend using **Up to Next Major Version**.

![Xcode Swift Package UI](docs/img/add-swift-package.png "Adding the Kouo SDK as a Swift package")

### Cocoapods

To import Kouo via Cocoapods, add this line to the relevant targets in your Podfile:

	import 'Kouo'

Then run `pod install --repo-update` from the command line.

## Capabilities and Entitlements

The Kouo SDK requires the following entitlements:

	com.apple.developer.healthkit
	com.apple.developer.healthkit.background-delivery

The Kouo SDK also requires the **background fetch** capability. This corresponds to the `fetch` entry in the `UIBackgroundModes` array in your Info.plist file.

You can add these entitlements and capabilities to your app in the **Signing & Capabilities** panel of the relevant Xcode target, as shown below:

![Entitlements and Capabilities required by the Kouo SDK](docs/img/entitlements-and-capabilities.png "Signing & Capabilities")

## Initializing the SDK

In your app delegate, import the Kouo module, and initialize the SDK using the API key that was provided to you, like this:

```swift
import Kouo
import UIKit

class MyAppDelegate: UIResponder, UIApplicationDelegate {
	let kouoAPIKey = "492d9c4c-017d-42fa-be60-b80f4b6fe3a9"

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		do {
			try KouoSDK.shared.setAPIKey(kouoAPIKey)
		} catch {
			debugPrint("Fail to initialize Kouo with \(error)")
		}

		return true
	}
}
```

## Registering a User

The SDK needs to associate any HealthKit data it collects with a specific user. So before any data collection can begin, you need to register a user with the SDK by providing a `username`.  This can be the user's email address, or any kind of handle, overt or opaque.  The important thing is that this username let you correlate any analytics data you collect in your app with emotional insights provided by Kouo's AI.

```swift
let myUsername = "john@doe.com"

KouoSDK.shared.setUsername(myUsername)
```

## Requesting Permission to Retrieve HealthKit Data

Kouo needs access to sensitive health data from the HealthKit store, and first of all, your app should check whether that data is available (it might not be available on some iOS devices).  This is done by using `isHealthDataAvailable`.

Once you have made sure that health data is available, you should check whether the user should be asked to grant your app permission to access the data.  The user may already have granted or denied permission in a previous session.  This is done by using `shouldRequestHealthKitAuthorization`.

If `shouldRequestHealthKitAuthorization` is `true`, then your app should request the user's permission using Kouo's `requestHealthKitAuthorization` method.  This will present the user with the standard HealthKit UI requesting access to the data types Kouo is interested in.  Be aware that these data types might change across versions of the SDK, so the user may need to grant additional permissions even if they already went through the permission panel in an earlier session.

Here's some sample code that puts all of the above together:

```swift
func requestKouoPermissions() {
	let kouo = KouoSDK.shared
	guard kouo.isHealthDataAvailable else { return }

	if kouo.shouldRequestHealthKitAuthorization {
		do {
			try kouo.requestHealthKitAuthorization()
		} catch {
			debugPrint("Failed to request HealthKit authorization with \(error)")
		}
	}
}
```

## Starting the Import Process

Once the SDK is initialized, the API key and username have been set, and any user permissions have been requested, you can finally start the collection process. This is done by calling the `start` method.

```swift
do {
	try KouoSDK.shared.start()
} catch {
	debugPrint("Failed to start Kouo SDK with \(error)")
}
```
The first time you call `start`, the Kouo SDK will query the HealthKit store for health samples dating back a long time, up to one year back.  After this initial ingestion, the Kouo SDK will schedule a long-running query that will keep acquiring samples as they become available, even when your app is in the background.

## Checking for Background Refresh

Kouo needs background refresh to be enabled for functioning properly while your app is in the background.  Your app should detect if background refresh has been turned off, and kindly ask the user to turn it on again, offering to open System Settings if they agree.  To check if background refresh is available, you can use this code:

```swift
var hasBackgroundRefresh: Bool {
	UIApplication.shared.backgroundRefreshStatus == .available
}
```

To open System Settings, you can use this code:

```swift
func openSystemSettings() {
	guard let systemSettingsURL = URL(string: UIApplication.openSettingsURLString) else { return }

	UIApplication.shared.open(systemSettingsURL, options: [:]) { success in
		if success {
			debugPrint("Successfully opened System Settings")
		} else {
			debugPrint("Failed to open System Settings")
		}
	}
}
```

## Getting Emotional Insights for Past Sessions

A **session** is a period of time in which the host app is in the foreground and interacting with the user.  The Kouo SDK automatically keeps tracks of sessions by detecting when the app goes in the background and returns to the foreground.  A log of all sessions is stored in a local database.

Your app can obtain emotion insights computed from health data collected during previous sessions, by calling the `getEmotionalInsightsForRecentSessions` method.  This method executes asynchronously, querying the local session log for past sessions, then the Kouo backend for emotional insights matching those sessions.  When it's finished, it returns its results through a callback that you provide.  Assuming the query was successful, it returns a list of `KouoEmotionalInsight` objects, which have the following structure:

```swift
public struct KouoEmotionalInsight: Codable {
	let timestamp: Date
	let focus: Double
	let energy: Double
	let stressAnxiety: Double
}
```

The `focus`, `energy`, and `stressAnxiety` values are on a range of 0 to 1.

Your callback should return a `bool` value: `true` if you have successfully processed the insight data, `false` if you have not.  Returning `true` instructs the SDK to mark the relevant sessions as *processed*, so they won't be considered again the next time you call `getEmotionalInsightsForRecentSessions`.

Here's how your app may want to retrieve emotional insights from the SDK:

```swift
KouoSDK.shared.getEmotionalInsightsForRecentSessions { [weak self] result in
	guard let self else { return false }

	switch result {
	case .success(let insights):
		return self.trackEmotionalInsights(insights)

	case .failure:
		return false
	}
}
```

Here we assume that you have a `trackEmotionalInsights` method that processes the insights in some app-specific way, and possibly tracks the insights using some analytics SDK, like Firebase Analytics, so they can be correlated with other events or screen views taking place at about the same time.

## Logging Emotional Insights to Firebase

If you are using Firebase to collect emotional insights from Kouo, you can use the `firebaseRepresentation` computed property of `KouoEmotionalInsight` to build a Firebase-ready dictionary that you can then log as a Firebase event, like this:

```swift
import FirebaseAnalytics

func trackEmotionalInsights(_ insights: [KouoEmotionalInsight]) -> Bool {
	insights.forEach { insight in
		Analytics.logEvent("KouoEmotionalInsight", parameters: insight.firebaseRepresentation)
	}

	return true
}
```
